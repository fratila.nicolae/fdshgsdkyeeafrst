package woo.lab2.nicu.examprep2.listAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import woo.lab2.nicu.examprep2.R;

public class GenreAdapter extends ArrayAdapter<String> {
    public GenreAdapter(Context context, int resource, List objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_client_request_item, parent, false);
        }
        TextView name = convertView.findViewById(R.id.genreItem);
        name.setText(getItem(position));

        return convertView;
    }
}
