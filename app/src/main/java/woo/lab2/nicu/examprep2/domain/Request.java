package woo.lab2.nicu.examprep2.domain;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;
import java.util.Objects;

@Entity
public class Request implements Serializable {
    @PrimaryKey
    private int id;
    private String name;
    private String product;
    private int quantity;
    private String status;

    @Override
    public String toString() {
        return "Request{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", product='" + product + '\'' +
                ", quantity=" + quantity +
                ", status='" + status + '\'' +
                '}';
    }

    public Request() {
    }

    public Request(int id, String name, String product, int quantity, String status) {
        this.id = id;
        this.name = name;
        this.product = product;
        this.quantity = quantity;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request request = (Request) o;
        return id == request.id &&
                quantity == request.quantity &&
                Objects.equals(name, request.name) &&
                Objects.equals(product, request.product) &&
                Objects.equals(status, request.status);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, product, quantity, status);
    }
}
