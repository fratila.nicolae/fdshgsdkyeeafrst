package woo.lab2.nicu.examprep2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import woo.lab2.nicu.examprep2.domain.Request;
import woo.lab2.nicu.examprep2.retrofit.RequestApi;

public class ClerkAddSong extends AppCompatActivity {

    private EditText nameField;
    private EditText productField;
    private EditText quantityField;
    private EditText statusField;
    private ProgressBar bar;
    private RequestApi requestApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clerk_add_song);

        setApis();

        nameField = findViewById(R.id.nameField);
        productField = findViewById(R.id.productField);
        quantityField = findViewById(R.id.quantityField);
        statusField = findViewById(R.id.statusField);
        bar = findViewById(R.id.addSongBar);

        Button addButton = findViewById(R.id.addButtonFinal);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addSong();
            }
        });

    }

    private void setApis() {
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl("http://172.30.113.213:2230/")
                .addConverterFactory(GsonConverterFactory.create()).build();

        requestApi = retrofit.create(RequestApi.class);
    }

    private void addSong() {
        String name = nameField.getText().toString();
        String product = productField.getText().toString();
        String status = statusField.getText().toString();
        Integer quantity = Integer.parseInt(quantityField.getText().toString());

        Request s = new Request(0,name, product, quantity, status);

        bar.setVisibility(View.VISIBLE);
        Log.d("[API]","[REQUEST][saveRequest]"+s.toString());
        requestApi.saveRequest(s).enqueue(new Callback<Request>() {
            @Override
            public void onResponse(Call<Request> call, Response<Request> response) {
                bar.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    Toast.makeText(getApplicationContext(),"The id of the added request is:" + response.body().getId(),Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(getApplicationContext(),response.code(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Request> call, Throwable t) {
                bar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_SHORT).show();

            }
        });

    }
}
