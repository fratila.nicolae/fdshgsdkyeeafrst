package woo.lab2.nicu.examprep2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import woo.lab2.nicu.examprep2.domain.Request;
import woo.lab2.nicu.examprep2.listAdapters.ClerkSongAdapter;
import woo.lab2.nicu.examprep2.retrofit.RequestApi;

public class ClerkSongs extends AppCompatActivity {

    private RequestApi requestApi;

    private ClerkSongAdapter songAdapter;
    private ListView listView;
    private ProgressBar bar;

    private void setApis() {
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl("http://172.30.113.213:2230/")
                .addConverterFactory(GsonConverterFactory.create()).build();

        requestApi = retrofit.create(RequestApi.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clerk_songs);

        songAdapter = new ClerkSongAdapter(getApplicationContext(), R.layout.activity_clerk_song_list_item, new ArrayList());

        setApis();

        listView = findViewById(R.id.clerk_songs);
        listView.setAdapter(songAdapter);
        bar = findViewById(R.id.clerkSongsBar);

        Button addSongButton = findViewById(R.id.addButton);
        addSongButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ClerkAddSong.class);
                startActivity(intent);
                populateSongs();
            }
        });

        Button topButton = findViewById(R.id.seeTopButton);
        topButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TopRequests.class);
                startActivity(intent);
            }
        });

        populateSongs();
    }

    private void populateSongs(){
        bar.setVisibility(View.VISIBLE);
        Log.d("[API]","[REQUEST][getClosedRequests]");
        requestApi.getClosedRequests().enqueue(new Callback<List<Request>>() {
            @Override
            public void onResponse(Call<List<Request>> call, Response<List<Request>> response) {
                bar.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    Collections.sort(response.body(), new Comparator<Request>() {
                        @Override
                        public int compare(Request i, Request j) {
                            return i.getQuantity().compareTo(j.getQuantity());
                        }
                    });
                    songAdapter.clear();
                    songAdapter.addAll(response.body());
                    return;
                }
                Toast.makeText(getApplicationContext(),response.code(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<Request>> call, Throwable t) {
                bar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }

}
