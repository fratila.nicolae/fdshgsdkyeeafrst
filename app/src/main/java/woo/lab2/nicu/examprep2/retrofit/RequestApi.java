package woo.lab2.nicu.examprep2.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import woo.lab2.nicu.examprep2.domain.Request;

public interface RequestApi {
    @GET("songs/{genre}")
    Call<List<Request>> getSongs(@Path("genre") String genre);

    @GET("big")
    Call<List<Request>> getBig();

    @GET("requests")
    Call<List<Request>> getAllRequests();

    @GET("closed")
    Call<List<Request>> getClosedRequests();

    @DELETE("request/{id}")
    Call<Void> deleteRequest(@Path("id") Integer songId);

    @POST("request")
    Call<Request> saveRequest(@Body Request request);

    @POST("fill")
    Call<Request> fullfillRequest(@Body Request id);
}
