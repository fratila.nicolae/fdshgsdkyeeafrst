package woo.lab2.nicu.examprep2;

import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;

import woo.lab2.nicu.examprep2.database.AppDatabase;
import woo.lab2.nicu.examprep2.listAdapters.FavoriteSongAdapter;

public class ClientFullfilled extends AppCompatActivity {

    private FavoriteSongAdapter songAdapter;

    private AppDatabase db;

    private ProgressBar bar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_favorites);

        songAdapter = new FavoriteSongAdapter(getApplicationContext(), R.layout.activity_client_request_item,new ArrayList());
        ListView listView = findViewById(R.id.favoritesList);
        listView.setAdapter(songAdapter);

        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "test-name").build();

        bar = findViewById(R.id.favoriteBar);

        new Thread(new Runnable() {
            @Override
            public void run() {
                bar.setVisibility(View.VISIBLE);
                Log.d("[DB]","[REQUEST][addAll]");
                songAdapter.addAll(db.requestDao().getRequests());
                bar.setVisibility(View.GONE);
            }
        }).start();


    }
}
