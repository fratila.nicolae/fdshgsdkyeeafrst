package woo.lab2.nicu.examprep2;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import woo.lab2.nicu.examprep2.domain.Request;
import woo.lab2.nicu.examprep2.listAdapters.ClientRequestsAdapter;
import woo.lab2.nicu.examprep2.retrofit.RequestApi;

public class ClientRequests extends AppCompatActivity {

    private ClientRequestsAdapter requestsAdapter;
    private ListView genreList;
    private ProgressBar bar;
    private LinearLayout retryBox;
    private RequestApi requestApi;
    private TextView errorMessage;

    private void setFields(){
        requestsAdapter = new ClientRequestsAdapter(getApplicationContext(),R.layout.activity_client_request_item,new ArrayList(),this);

        bar = findViewById(R.id.progressBar);
        genreList = findViewById(R.id.genreList);
        genreList.setAdapter(requestsAdapter);

        retryBox = findViewById(R.id.retryBox);
        errorMessage = findViewById(R.id.errorMessage);



        setRetryButton();
    }

    private void setApis(){
        Retrofit retrofit = new Retrofit
                .Builder()
                    .baseUrl("http://172.30.113.213:2230/")
                .addConverterFactory(GsonConverterFactory.create()).build();

        requestApi = retrofit.create(RequestApi.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supplier_requests);

        setFields();
        setApis();

        fillGenreList();

        Button favoritesButton = findViewById(R.id.fillButton);
        favoritesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),ClientFullfilled.class);
                startActivity(intent);
            }
        });

    }

    private void fillGenreList(){

        bar.setVisibility(View.VISIBLE);

        Log.d("[API]","[REQUEST][getAllRequests]");
        requestApi.getAllRequests().enqueue(new Callback<List<Request>>() {
            @Override
            public void onResponse(Call<List<Request>> call, Response<List<Request>> response) {
                bar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    requestsAdapter.clear();
                    requestsAdapter.addAll(response.body());
                    return;
                }
                errorMessage.setText(response.code());
                retryBox.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<List<Request>> call, Throwable t) {
                bar.setVisibility(View.GONE);
                errorMessage.setText(t.getMessage());
                retryBox.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setRetryButton(){
        final Button retryButton = findViewById(R.id.retryButton);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryBox.setVisibility(View.GONE);
                fillGenreList();
            }
        });
    }



    public boolean isOnline() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
