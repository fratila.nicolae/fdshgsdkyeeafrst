package woo.lab2.nicu.examprep2.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GenreApi {
    @GET("genres")
    Call<List<String>> getGenres();
}
