package woo.lab2.nicu.examprep2.listAdapters;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import woo.lab2.nicu.examprep2.ClientRequests;
import woo.lab2.nicu.examprep2.R;
import woo.lab2.nicu.examprep2.database.AppDatabase;
import woo.lab2.nicu.examprep2.domain.Request;
import woo.lab2.nicu.examprep2.retrofit.RequestApi;

public class ClientRequestsAdapter extends ArrayAdapter<Request> {


    private RequestApi requestApi;
    private ClientRequests clientRequests;
    private AppDatabase db;

    public ClientRequestsAdapter(Context context, int resource, List objects, ClientRequests clientRequests) {
        super(context, resource, objects);
        setApis();
        this.clientRequests = clientRequests;
        db = Room.databaseBuilder(getContext(),
                AppDatabase.class, "test-name").build();

    }

    private void setApis() {
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl("http://172.30.113.213:2230/")
                .addConverterFactory(GsonConverterFactory.create()).build();

        requestApi = retrofit.create(RequestApi.class);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_client_request_item, parent, false);
        }

        TextView name = convertView.findViewById(R.id.crName);
        name.setText(getItem(position).getName());
        TextView album = convertView.findViewById(R.id.crProduct);
        album.setText(getItem(position).getProduct());
        TextView genre = convertView.findViewById(R.id.crQuantity);
        genre.setText(getItem(position).getQuantity().toString());
        Button deleteButton = convertView.findViewById(R.id.fullfillButton);
        final ProgressBar bar = convertView.findViewById(R.id.fullfillBar);

        final Request request = getItem(position);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clientRequests.isOnline()) {
                    bar.setVisibility(View.VISIBLE);

                    Log.d("[API]","[REQUEST][fullfillRequest]" + request.toString());
                    requestApi.fullfillRequest(request).enqueue(new Callback<Request>() {
                        @Override
                        public void onResponse(Call<Request> call, Response<Request> response) {
                            bar.setVisibility(View.GONE);
                            if (response.isSuccessful()) {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {

                                        Log.d("[DB]","[REQUEST][saveRequest]" + request.toString());
                                        db.requestDao().saveRequest(request);
                                    }
                                }).start();
                                remove(request);
                                return;
                            }
                            Toast.makeText(getContext(), response.code(), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<Request> call, Throwable t) {
                            bar.setVisibility(View.GONE);
                            Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    Toast.makeText(getContext(), "No connection", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return convertView;
    }
}
