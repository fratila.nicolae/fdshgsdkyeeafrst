package woo.lab2.nicu.examprep2;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import woo.lab2.nicu.examprep2.domain.Request;
import woo.lab2.nicu.examprep2.listAdapters.ClerkSongAdapter;
import woo.lab2.nicu.examprep2.listAdapters.SongAdapter;
import woo.lab2.nicu.examprep2.retrofit.RequestApi;

import static java.lang.Math.min;

public class TopRequests extends AppCompatActivity {

    private ClerkSongAdapter songAdapter;
    private RequestApi requestApi;
    private ListView listView;
    private ProgressBar progressBar;

    private void setApis() {
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl("http://172.30.113.213:2230/")
                .addConverterFactory(GsonConverterFactory.create()).build();

        requestApi = retrofit.create(RequestApi.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_requests);


        setApis();

        listView = findViewById(R.id.clientSongsList);
        songAdapter = new ClerkSongAdapter(getApplicationContext(), R.layout.activity_clerk_song_list_item, new ArrayList());
        listView.setAdapter(songAdapter);

        progressBar = findViewById(R.id.clientSongsBar);

        populateList();
    }

    private void populateList() {
        progressBar.setVisibility(View.VISIBLE);


        Log.d("[API]","[REQUEST][getBig]");
        requestApi.getBig().enqueue(new Callback<List<Request>>() {
            @Override
            public void onResponse(Call<List<Request>> call, Response<List<Request>> response) {


                List<Request> requests = response.body();

                List<Request> newList = new ArrayList<>();



                Collections.sort(requests, new Comparator<Request>() {
                    @Override
                    public int compare(Request request, Request t1) {
                        return t1.getQuantity().compareTo(request.getQuantity());
                    }
                });

                for (int i = 0; i< min(10,requests.size()); i++){
                    newList.add(requests.get(i));
                }

                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    songAdapter.clear();
                    songAdapter.addAll(newList);
                    return;
                }
                Toast.makeText(getApplicationContext(),response.code(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<Request>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }
}
