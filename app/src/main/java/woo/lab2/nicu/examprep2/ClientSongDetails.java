package woo.lab2.nicu.examprep2;

import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import woo.lab2.nicu.examprep2.database.AppDatabase;
import woo.lab2.nicu.examprep2.domain.Request;

public class ClientSongDetails extends AppCompatActivity {

    private Request request;

    private AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_song_details);

        request = (Request) getIntent().getBundleExtra("bundle").getSerializable("request");

        setTextFields();

        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "test-name").build();

        Button button = findViewById(R.id.favoriteButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
//                        db.songDao().saveRequest(request);
                    }
                }).start();
            }
        });
    }

    private void setTextFields() {
//        TextView title = findViewById(R.id.cdTitle);
//        title.setText(request.getTitle());
//        TextView description = findViewById(R.id.cdDescription);
//        description.setText(request.getDescription());
//        TextView album = findViewById(R.id.cdAlbum);
//        album.setText(request.getAlbum());
//        TextView genre = findViewById(R.id.cdGenre);
//        genre.setText(request.getGenre());
//        TextView year = findViewById(R.id.cdYear);
//        year.setText(request.getYear().toString());
    }
}
