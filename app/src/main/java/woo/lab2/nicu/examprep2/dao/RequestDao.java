package woo.lab2.nicu.examprep2.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import woo.lab2.nicu.examprep2.domain.Request;

@Dao
public interface RequestDao {

    @Query("SELECT * FROM Request")
    List<Request> getRequests();

    @Insert
    void saveRequest(Request request);
}
