package woo.lab2.nicu.examprep2.listAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import woo.lab2.nicu.examprep2.R;
import woo.lab2.nicu.examprep2.domain.Request;

public class FavoriteSongAdapter extends ArrayAdapter<Request> {
    public FavoriteSongAdapter(Context context, int resource, List objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_client_request_item, parent, false);
        }

        TextView name = convertView.findViewById(R.id.crName);
        name.setText(getItem(position).getName());

        TextView quantity = convertView.findViewById(R.id.crQuantity);
        quantity.setText(getItem(position).getQuantity().toString());

        TextView product = convertView.findViewById(R.id.crProduct);
        product.setText(getItem(position).getProduct());


        return convertView;
    }
}
