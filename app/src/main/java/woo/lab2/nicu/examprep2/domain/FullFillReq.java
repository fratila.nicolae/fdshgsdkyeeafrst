package woo.lab2.nicu.examprep2.domain;

public class FullFillReq {
    private Integer id;


    public FullFillReq() {
    }

    public FullFillReq(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
