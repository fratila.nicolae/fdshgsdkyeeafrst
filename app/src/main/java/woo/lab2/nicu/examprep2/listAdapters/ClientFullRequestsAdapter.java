package woo.lab2.nicu.examprep2.listAdapters;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import woo.lab2.nicu.examprep2.ClientRequests;
import woo.lab2.nicu.examprep2.R;
import woo.lab2.nicu.examprep2.database.AppDatabase;
import woo.lab2.nicu.examprep2.domain.Request;
import woo.lab2.nicu.examprep2.retrofit.RequestApi;

public class ClientFullRequestsAdapter extends ArrayAdapter<Request> {

    public ClientFullRequestsAdapter(Context context, int resource, List objects) {
        super(context, resource, objects);
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_client_song_list_item, parent, false);
        }

        TextView name = convertView.findViewById(R.id.crName);
        name.setText(getItem(position).getName());

        TextView quantity = convertView.findViewById(R.id.crQuantity);
        quantity.setText(getItem(position).getQuantity().toString());

        TextView product = convertView.findViewById(R.id.crProduct);
        product.setText(getItem(position).getProduct());

        return convertView;
    }
}
