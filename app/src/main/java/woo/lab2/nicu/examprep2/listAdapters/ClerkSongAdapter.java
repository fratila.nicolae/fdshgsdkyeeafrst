package woo.lab2.nicu.examprep2.listAdapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import woo.lab2.nicu.examprep2.R;
import woo.lab2.nicu.examprep2.domain.Request;
import woo.lab2.nicu.examprep2.retrofit.RequestApi;

public class ClerkSongAdapter extends ArrayAdapter<Request> {


    private RequestApi requestApi;

    public ClerkSongAdapter(Context context, int resource, List objects) {
        super(context, resource, objects);
        setApis();
    }

    private void setApis() {
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl("http://172.30.113.213:2230/")
                .addConverterFactory(GsonConverterFactory.create()).build();

        requestApi = retrofit.create(RequestApi.class);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_clerk_song_list_item, parent, false);
        }

        TextView name = convertView.findViewById(R.id.ciName);
        name.setText(getItem(position).getName());
        TextView album = convertView.findViewById(R.id.ciQuantity);
        album.setText(getItem(position).getQuantity().toString());
        Button deleteButton = convertView.findViewById(R.id.deleteButton);
        final ProgressBar bar = convertView.findViewById(R.id.deleteBar);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bar.setVisibility(View.VISIBLE);
                Log.d("[API]","[REQUEST][deleteRequest]"+getItem(position).getId());
                requestApi.deleteRequest(getItem(position).getId()).enqueue(new Callback() {
                    @Override
                    public void onResponse(Call call, Response response) {
                        bar.setVisibility(View.GONE);
                        if (response.isSuccessful()){
                            remove(getItem(position));
                            return;
                        }
                        Toast.makeText(getContext(),response.code(),Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call call, Throwable t) {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(getContext(),t.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        return convertView;
    }
}
