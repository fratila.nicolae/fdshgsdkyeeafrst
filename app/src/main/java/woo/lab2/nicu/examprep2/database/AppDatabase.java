package woo.lab2.nicu.examprep2.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import woo.lab2.nicu.examprep2.dao.RequestDao;
import woo.lab2.nicu.examprep2.domain.Request;

@Database(entities = {Request.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
    public abstract RequestDao requestDao();
}
