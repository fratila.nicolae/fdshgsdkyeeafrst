package woo.lab2.nicu.examprep2.listAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import woo.lab2.nicu.examprep2.R;
import woo.lab2.nicu.examprep2.domain.Request;

public class SongAdapter extends ArrayAdapter<Request> {
    public SongAdapter(Context context, int resource, List objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_client_song_list_item, parent, false);
        }
//        TextView name = convertView.findViewById(R.id.clientSongTitle);
//        name.setText(getItem(position).getTitle());
//        TextView album = convertView.findViewById(R.id.clientSongAlbum);
//        album.setText(getItem(position).getAlbum());

        return convertView;
    }
}
